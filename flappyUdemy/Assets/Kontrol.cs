﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Kontrol : MonoBehaviour
{
    public Sprite[] birdSprites;
    private SpriteRenderer spriteRenderer;
    public bool ileriGeriKontrol=true;
    public int kusSayac=0;
    int puan=0;
    public float kusAnimasyonZaman=0;
    public Rigidbody2D myRigid;
    public Text puanText;
    bool oyunBittiMi=false;

    OyunKontrol oyunKontrol;
    AudioSource[] sesler;

   
    int enYuksekPuan=0;
     
    

 
    void Start()
    {
        spriteRenderer=GetComponent<SpriteRenderer>();
        myRigid=GetComponent<Rigidbody2D>();
        oyunKontrol=GameObject.FindGameObjectWithTag("oyunkontrol").GetComponent<OyunKontrol>(); // Tag ile oyunkontrol scriptine ulaşıyoruz.
        sesler=GetComponents<AudioSource>(); //Arayüzde eklediğimiz seslere erişmek için GetComponents kullanıyoruz.
        enYuksekPuan=PlayerPrefs.GetInt("Kayit"); //Aşağıda Set ettiğimiz veriyi çekiyoruz.
       

        
        
        
    }


    void Update()
    {
        if(Input.GetMouseButtonDown(0)&& !oyunBittiMi) //Fareye tıklandığı ve oyun bitmediği sürece çalışacak.
        {
            myRigid.velocity=new Vector2(0,0); //hizi sıfır yaptıktan sonra kuvvet uygular.
            myRigid.AddForce(new Vector2(0,200));//Dikey yönde zıplama kuvveti.
            sesler[0].Play(); //Kanat çırpma sesini çal.
           

        }

        if(myRigid.velocity.y>0) //Kuşu z ekseninde 45 derece yukarı baktırıyoruz.
        {
            transform.eulerAngles=new Vector3(0,0,45);

        }

        else // y<0 ise kuş aşağı yöne bakacak şekilde ayarlanır.
        {
            transform.eulerAngles=new Vector3(0,0,-45);
        }
        Animasyon();
    }

       

    void Animasyon() {

    kusAnimasyonZaman+= Time.deltaTime;
            
            if(kusAnimasyonZaman>0.4f){
            kusAnimasyonZaman=0;
            
            if(ileriGeriKontrol){
                spriteRenderer.sprite=birdSprites[kusSayac];
                kusSayac++;
                
                if(kusSayac==birdSprites.Length){
                    kusSayac--;
                    ileriGeriKontrol=false;
                }

            }

            else {
                kusSayac--;
                spriteRenderer.sprite=birdSprites[kusSayac];
                
                if(kusSayac==0){
                    kusSayac++;
                    ileriGeriKontrol=true;
                }

            }



            
            
        }
    }

    
    void OnTriggerEnter2D(Collider2D col) //Trigger, bir nesneye çarpınca tetiklenicek  fonksiyon.
    {
        if(col.gameObject.tag=="puan")
        {
            puan++; 
            puanText.text="Score =" +puan;
            sesler[1].Play(); //Skor alınca oynatılcak olan ses.
            

        }

        if(col.gameObject.tag=="engel")
        {
            oyunBittiMi=true; //Oyun bitecek.
            sesler[2].Play(); //Çarpma sesi oynatılacak.
            oyunKontrol.oyunBitti();
            GetComponent<CircleCollider2D>().enabled=false; //Kuş çarpma işlemini gerçekleştirdiğinde sadece 1 kez çarpma sesinin çalması için Collider disable yapılır.
            

            if(puan>enYuksekPuan)
            {
                enYuksekPuan=puan;
                PlayerPrefs.SetInt("Kayit",enYuksekPuan); //Hafızasına kayıt eder.

            }
            Invoke("anaMenuyeDon",2); // 2sn sonra anamenüye dön. Parametre olarak çağrılacak metodun ismini veriyoruz ve kaç sn sonra çağrılacağının bilgisini veriyoruz.


        }
        

    }

    void anaMenuyeDon()
    {
        PlayerPrefs.SetInt("puanKayit",puan);
        SceneManager.LoadScene("anaMenu"); //anaMenu ekranına yükleme işlemi.
      

    }

   

    
 }
    





