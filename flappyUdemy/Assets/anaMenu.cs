﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // Sahneler arasında geçiş yapmak için tanımlıyoruz.
using UnityEngine.UI;

public class anaMenu : MonoBehaviour
{
    public Text puanText; //En yüksek puan
    public Text puan;
   
    void Start()
    {
        int enYuksekSkor=PlayerPrefs.GetInt("Kayit");
        int puanGelen=PlayerPrefs.GetInt("puanKayit");
        puanText.text="En Yuksek Skor = "+enYuksekSkor;
        puan.text="Puan ="+puanGelen;
        
    }

   
    void Update()
    {
        
    }

    public void oyunaGit()
    {
        SceneManager.LoadScene("level");

    }

    public void oyundanCik()
    {
        Application.Quit();

    }
}
