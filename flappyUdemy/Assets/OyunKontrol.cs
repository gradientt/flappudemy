﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OyunKontrol : MonoBehaviour
{
    public GameObject gokyuzuBir;
    public GameObject gokyuzuIki;
    public float arkaPlanhiz=-1.5f;

    Rigidbody2D myRigid1;
    Rigidbody2D myRigid2;

     float uzunluk=0;
    public GameObject engel;
    public int kacAdetEngel;
    public GameObject[] engeller;
    
    public float zamanDegisimi=0;
    int sayac=0;
    bool oyunbitmedi=true;


    
    void Start()
    {
        myRigid1=gokyuzuBir.GetComponent<Rigidbody2D>();
        myRigid2=gokyuzuIki.GetComponent<Rigidbody2D>();

        myRigid1.velocity=new Vector2(arkaPlanhiz,0);
        myRigid2.velocity=new Vector2(arkaPlanhiz,0);

        uzunluk=gokyuzuBir.GetComponent<BoxCollider2D>().size.x; //Gokyüzü1' in uzunluğunu alıyoruz.

        engeller= new GameObject[kacAdetEngel];
        InvokeRepeating("callMe",2.0f,5.0f); //callMe adindaki methodu çağır , oyun başladıktan kaç saniye sonra çağılacağını yazıyoruz ve kaç saniyede bir çağrılacağını belirtiyoruz.

       
        
       
      

        for (int i = 0; i < engeller.Length; i++)
        {
            //Instantiate methodu verilen prefab veya objeyi girilen kordinatlarda oluşturmamızı sağlar.
           engeller[i]=Instantiate(engel,new Vector2(-20,-20),Quaternion.identity); //Engelleri obje dizisine atıyoruz. Quaternion.Identity rotation'ı temsil eder.
           Rigidbody2D fizikEngel= engeller[i].AddComponent<Rigidbody2D>(); //Arayüzden GetComponent ile çağırmak yerine burada objeye RigidBody2D ekliyoruz.
           fizikEngel.gravityScale=0; 
           fizikEngel.velocity=new Vector2(arkaPlanhiz,0); 
        }

          
        

        
        
    }

    
    void Update()
    {
        if(oyunbitmedi) 
        {
          
            if(gokyuzuBir.transform.position.x  <= -uzunluk)
        {
            gokyuzuBir.transform.position += new Vector3(uzunluk*2,0);
        }

        
         if(gokyuzuIki.transform.position.x  <= -uzunluk)
        {
            gokyuzuIki.transform.position += new Vector3(uzunluk*2,0);
        }

        //---
        zamanDegisimi += Time.deltaTime;
        if(zamanDegisimi>1.3f) //Engellerin oluşma süresi.
        {
            zamanDegisimi=0;
            float Yeksenim=Random.Range(-0.50f,1.10f); //Engellerim y ekseni içerisinde random oluşacak ,oyun alanı sınırları içerisindeki max ve min değere göre.
            engeller[sayac].transform.position=new Vector3(18,Yeksenim);
            sayac++;

            if(sayac>=engeller.Length)
            {
                sayac=0;

            }
        }



          

         }



    
        
    }

 
    public void oyunBitti()
    {
        for (int i = 0; i < engeller.Length; i++)
        {
            engeller[i].GetComponent<Rigidbody2D>().velocity=Vector2.zero; //Tanımlanan tüm engellerin hızı x ve y ekseninde 0 olacak.
            //Gokyüzü 1 ve Gokyüzü 2 objelerimizin x ve y ekseninde hızlarını sıfırlıyoruz.
            myRigid1.velocity=Vector2.zero;
            myRigid2.velocity=Vector2.zero;
            
            
            
        }
       oyunbitmedi=false;
       
    }

    void callMe()
    {
        arkaPlanhiz -= 0.5f;
        Hizlandir(arkaPlanhiz);

    }

    void Hizlandir(float hiz)
    {
        if(oyunbitmedi)
        {

            myRigid1.velocity=new Vector2(hiz,0);
            myRigid2.velocity=new Vector2(hiz,0);

            for (int i = 0; i < engeller.Length; i++)
            {
                Rigidbody2D fizikEngel=engeller[i].GetComponent<Rigidbody2D>();
                fizikEngel.velocity=new Vector2(hiz,0);
                
            }






        }

    }

        
        
    }

    
